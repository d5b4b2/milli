# Date format converter

Convert between Epoch-milli and ISO date formats and vice versa.

## Example

```
$ milli 2018-09-28T14:40:00+02:00
1538138400000

$ milli 2018-09-28T14:40:00
1538138400000

$ milli 2018-09-28
1538085600000

$ milli 1538138400000
2018-09-28T14:40:00+02:00
```

## Usage

```
usage: milli [EPOCH...] [ISO...]
```
