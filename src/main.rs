// Copyright 2018-2019 Gerrit Viljoen

// This file is part of milli.
//
// milli is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// milli is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with milli.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;
use chrono::{DateTime, TimeZone, Local};

use std::env::args;

fn main() {
    for arg in args().skip(1) {
        if let Ok(milli) = arg.parse::<i64>() {
            println!("{}", Local.timestamp_millis(milli).to_rfc3339())
        } else if let Ok(date) = DateTime::parse_from_rfc3339(&arg) {
            println!("{}", date.timestamp_millis())
        } else if let Ok(date) = Local.datetime_from_str(&arg, "%FT%T") {
            println!("{}", date.timestamp_millis())
        } else if let Ok(date) = Local.datetime_from_str(&format!("{}T00:00:00", arg), "%FT%T") {
            println!("{}", date.timestamp_millis())
        } else {
            panic!("Unknown date/milli `{}`", arg)
        }
    }
}
